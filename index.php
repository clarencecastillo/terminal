<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Clarence Castillo</title>


        <!--        
        Bootstrap v3.1.1
        Copyright 2014 Twitter, Inc
        Designed and built with all the love in the world by @mdo and @fat.
        Code licensed under MIT, documentation under CC BY 3.0.
        -->

        <!-- Bootstrap core CSS -->
        <link href="bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="bootstrap-3.1.1-dist/css/cover.css" rel="stylesheet">

    </head>

    <body>

        <div class="site-wrapper">

            <div class="site-wrapper-inner">

                <div class="cover-container">
                    <div class="inner cover">
                        <h1 class="cover-heading">Under Construction</h1>
                        <p class="lead">Hello there. Unfortunately, I'm still getting my head around this amazing framework, so it might take awhile for my site to get fixed. Come back again soon. Like really, really soon.<br><br>Seriously.</p>
                    </div>

                    <div class="mastfoot">
                        <div class="inner">
                            <p>Powered by <a href="https://www.openshift.com/"><img src='http://mirror.openshift.com/pub/openshift/logo/favicon-32.png'/><b>OPEN</b>SHIFT</a></p>
                            <p>Cover template for <a href="http://getbootstrap.com">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                            <p class='small'>© Clarence Castillo 2014 All rights reserved.</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </body>
</html>
